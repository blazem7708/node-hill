const PacketBuilder = require("../../util/net/packetBuilder").default

const { hexToDec } = require("../../util/color/color")

function createTeamPacket(team) {
    const packet = new PacketBuilder("Team")
        .write("uint32", team.netId)
        .write("string", team.name)
        .write("uint32", hexToDec(team.color))
    return packet
}

module.exports = createTeamPacket